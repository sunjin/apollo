package com.example;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Tests for correct dependency retrieval with maven rules
 */

public class TestMain {

    @Test
    public void testHello() throws Exception {

        assertEquals("helloだよ", Main.sayHello(), "hello");
    }

}