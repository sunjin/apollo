package com.example;

import com.example.router.Router;
import com.spotify.apollo.httpservice.HttpService;
import com.spotify.apollo.httpservice.LoadingException;

public class Main {
    public static void main(String args[]) throws LoadingException {
        System.out.println("==== Start Apollo Server ====");

        // Server Start
        HttpService.boot(Router::init, "my-first-app", args);
    }

    public static String sayHello() {
        
        return "hello";
    }

}
