package com.example.router;

import com.example.controller.Controller;
import com.example.router.hello.HelloRouter;
import com.google.gson.Gson;
import com.spotify.apollo.Environment;
import com.spotify.apollo.RequestContext;
import com.spotify.apollo.Response;
import com.spotify.apollo.route.Route;

import java.util.Map;

public class Router {

    private static Controller controller = new Controller();

    public interface Method {
        String GET = "GET";
        String POST = "POST";
        String PUT = "PUT";
        String DELETE = "DELETE";
    }

    /**
     * router
     * 
     * @param environment
     */
    public static void init(Environment environment) {

//        environment.routingEngine().registerAutoRoute(Route.sync("GET", "/", rc -> "hello world"));
//        environment.routingEngine().registerAutoRoute(Route.sync("GET", "/bye", rc -> "good bye"));
//        environment.routingEngine()
//                .registerAutoRoute(Route.sync("GET", "/mstitem", rc -> toJson(controller.getMstItemList())));

        environment.routingEngine()
                .registerAutoRoute(Route.sync("GET", "/", rc -> "hello world"))
                .registerAutoRoute(Route.sync("GET", "/bye", rc -> "good bye"))
                .registerAutoRoutes(new MyResource())
                .registerAutoRoutes(new HelloRouter());


    }

    /**
     * jsonにする
     * 
     * @param response
     * @return
     */
    private static String toJson(Object response) {
        Gson gson = new Gson();
        return gson.toJson(response);
    }

}