package com.example.router.hello;

import com.example.router.Router;
import com.example.service.HelloService;
import com.spotify.apollo.route.AsyncHandler;
import com.spotify.apollo.route.Route;
import com.spotify.apollo.route.RouteProvider;

import java.util.stream.Stream;

public class HelloRouter implements RouteProvider {

    // service
    private static HelloService helloService;

    public static HelloService getHelloService() {
        if (helloService == null) {
            helloService = new HelloService();
        }
        return helloService;
    }


    @Override
    public Stream<? extends Route<? extends AsyncHandler<?>>> routes() {

        return Stream.of(
            Route.sync(Router.Method.GET, "/hello", rc -> getHelloService().hello())
        );
    }
}
