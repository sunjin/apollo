package com.example.router;

import com.spotify.apollo.route.AsyncHandler;
import com.spotify.apollo.route.Route;
import com.spotify.apollo.route.RouteProvider;

import java.util.stream.Stream;

/**
 * ルーティングをこんな風に指定できる
 */
public class MyResource implements RouteProvider {

    @Override
    public Stream<? extends Route<? extends AsyncHandler<?>>> routes() {
        return Stream.of(
                Route.sync(Router.Method.GET, "/test", requestContext -> "test"),
                Route.sync(Router.Method.GET, "/test2", requestContext -> "test2")
        );
    }
}
