package com.example.model;


public class MST_ITEM {

    private String id;

    private int ino;

//    @JsonProperty("name")
//    private String name;

    private String kind;

    private int itpno;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIno() {
        return ino;
    }

    public void setIno(int ino) {
        this.ino = ino;
    }

//    @JsonProperty("name")
//    public String getName() {
//        return name;
//    }
//
//    @JsonProperty("name")
//    public void setName(String name) {
//        this.name = name;
//    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public int getItpno() {
        return itpno;
    }

    public void setItpno(int itpno) {
        this.itpno = itpno;
    }
}