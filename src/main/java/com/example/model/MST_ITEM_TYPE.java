package com.example.model;


public class MST_ITEM_TYPE {


    private int tpnoInt;

    private long tpno;

    private String type;

    public long getTpno() {
        return tpno;
    }

    public void setTpno(long tpno) {
        this.tpno = tpno;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
