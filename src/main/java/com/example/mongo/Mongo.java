package com.example.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

import org.bson.Document;

/**
 * MongoとのConnectionをする
 */
public class Mongo {

    // mongo access uri
    private final static String URI = "mongodb://coconejp:cocone1221@vita-alpha-0.cocone:17071/VITA";

    /**
     * MongoClientURIを取得
     * 
     * @return
     */
    private MongoClientURI mongoClientURI() {
        return new MongoClientURI(URI);
    }

    /**
     * MongoCLientを取得
     * 
     * @return
     */
    public MongoClient mongoClient() {
        return new MongoClient(this.mongoClientURI());
    }

    /**
     * MongoDBを取得
     * 
     * @param dbName
     * @return
     */
    public MongoDatabase mongoDb(String dbName) {
        return this.mongoClient().getDatabase(dbName);
    }

    /**
     * Documentを取得
     * 
     * @param dbName
     * @param colName
     * @return
     */
    public MongoCollection<Document> getMongoDocuments(String dbName, String colName) {
        return this.mongoDb(dbName).getCollection(colName);
    }

}