package com.example.controller;

import java.util.*;

import com.example.mongo.Mongo;

import com.google.gson.Gson;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;

import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class Controller {

    // private final Gson gson = new Gson();
    private static Gson gson;

    public static Gson getGson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    /**
     * MST_ITEMを取得してみる
     * 
     * @return
     */
    public Map<String, Object> getMstItemList() {

        Map<String, Object> resultMap = new HashMap<>();

        resultMap.put("hello", "world");
        resultMap.put("me", "good job");
        String json = getGson().toJson(resultMap);
        System.out.println(json);
        return resultMap;
    }

    private MongoCollection<Document> getCollection(Mongo mongo, String dbName, String colName) {

        MongoClient mongoClient = mongo.mongoClient();
        MongoDatabase db = mongoClient.getDatabase(dbName);
        return db.getCollection(colName);
    }

}